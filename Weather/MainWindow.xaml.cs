﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Timers;
using System.Diagnostics;
//using System.Threading;
using System.Threading;

namespace Weather
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static HtmlWeb hw = new HtmlWeb();
        public static HtmlDocument hd = hw.Load("https://yandex.ru/pogoda/berlin/details");
        public static List<int> TempValues5days = new List<int>();

        public MainWindow()
        {
            InitializeComponent();
            var gostFont = new FontFamily(@"file:..\..\Assets\Font\GOST-type-B-Regular.ttf");
            System.Timers.Timer myTimer = new System.Timers.Timer(1000 * 10 * 60);
            myTimer.Elapsed += myTimerTick;

            myTimer.Start();
            Update();
        }

        public void myTimerTick(object source, EventArgs e)
        {
            this.Dispatcher.Invoke(() => { Update(); });
            
        }
        public void Update()
        {
            CurrentDate_TextBlock.Text = DateTime.Now.ToShortDateString();
            DateTime day1 = DateTime.Now.AddDays(1);
            D1Date_TextBlock.Text = day1.ToShortDateString();
            DateTime day2 = DateTime.Now.AddDays(2);
            D2Date_TextBlock.Text = day2.ToShortDateString();
            DateTime day3 = DateTime.Now.AddDays(3);
            D3Date_TextBlock.Text = day3.ToShortDateString();
            DateTime day4 = DateTime.Now.AddDays(4);
            D4Date_TextBlock.Text = day4.ToShortDateString();
            Updated_TextBlock.Text = "Last update: " + DateTime.Now.ToShortTimeString();
            Updated_TextBlock.Foreground = new SolidColorBrush(Colors.Green);
            Updated_TextBlock.SetValue(TextBlock.FontWeightProperty, FontWeights.Bold);
            System.Timers.Timer greenTextTimer = new System.Timers.Timer();
            greenTextTimer.Interval = 1000;
            greenTextTimer.Elapsed += new ElapsedEventHandler(greenTextTimerElapsed);
            greenTextTimer.Enabled = true;
            
            UpdateDayTempRange();
            UpdateCurrSkyImg();
            UpdateTempForecast();
            UpdateWPForecast();
            UpdateWind();
        }

        

        public void greenTextTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                Updated_TextBlock.Foreground = new SolidColorBrush(Colors.Gray);
                Updated_TextBlock.SetValue(TextBlock.FontWeightProperty, FontWeights.Normal);
            });
            
        }

        public void UpdateDayTempRange()
        {
            string st_nodes = "";
            
            HtmlNodeCollection tempRange = hd.DocumentNode.SelectNodes("//td[@class='weather-table__body-cell weather-table__body-cell_type_daypart']");

            for (int i = 1; i < 21; i++)
            {
                Regex regex = new Regex(@"((\+|\-)*\d+&hellip;(\+|\-)*\d+)|((\+|\-)*\d+)", RegexOptions.Singleline);
                Match match = regex.Match(tempRange[i - 1].InnerText.ToString());

                Regex regex1 = new Regex(@"(\+|\-)\d+");
                MatchCollection matches = regex1.Matches(match.Value);

                if (matches.Count > 1)
                {
                    if (i % 4 == 0)
                        st_nodes += matches[0].Value + "..." + matches[1].Value + "\n\n";
                    else
                        st_nodes += matches[0].Value + "..." + matches[1].Value + "\n";
                    TempValues5days.Add(Convert.ToInt32(matches[0].Value));
                    TempValues5days.Add(Convert.ToInt32(matches[1].Value));
                }
                else
                {
                    if (i % 4 == 0)
                        st_nodes += matches[0].Value + "\n\n";
                    else
                        st_nodes += matches[0].Value + "\n";
                    TempValues5days.Add(Convert.ToInt32(matches[0].Value));
                    TempValues5days.Add(Convert.ToInt32(matches[0].Value));
                }
            }
            int[] tempRangeToday = new int[]
            {
                TempValues5days[0],
                TempValues5days[1],
                TempValues5days[2],
                TempValues5days[3],
                TempValues5days[4],
                TempValues5days[5],
                TempValues5days[6],
                TempValues5days[7],
            };
            int maxTempToday = tempRangeToday.Max();
            int minTempToday = tempRangeToday.Min();
            TempDay_TextBlock.Text = String.Format("{0:+0;-0;0}...{1:+0;-0;0} °C", minTempToday, maxTempToday);
            double avgTempToday = tempRangeToday.Average();
            if (avgTempToday < -10)
            {
                TempDay_Image.Source = new BitmapImage(new Uri(@"Assets/pics/thermo1.png", UriKind.Relative));
            }
            else
                if (avgTempToday < 10)
            {
                TempDay_Image.Source = new BitmapImage(new Uri(@"Assets/pics/thermo2.png", UriKind.Relative));
            }
            else
                if (avgTempToday < 25)
            {
                TempDay_Image.Source = new BitmapImage(new Uri(@"Assets/pics/thermo3.png", UriKind.Relative));
            }
            else
                if (avgTempToday < 35)
            {
                TempDay_Image.Source = new BitmapImage(new Uri(@"Assets/pics/thermo4.png", UriKind.Relative));
            }
            else TempDay_Image.Source = new BitmapImage(new Uri(@"Assets/pics/thermo5.png", UriKind.Relative));
            //foreach (var n in tempRange)
            //{
            //    Regex regex = new Regex(@"((\+|\-)*\d+&hellip;(\+|\-)*\d+)|((\+|\-)*\d+)", RegexOptions.Singleline);
            //    Match match = regex.Match(n.InnerText.ToString());
            //    count++;                          
            //    //count++;                       //works fine. Uncomment if needed.
            //    //if(count % 4 == 0)
            //    //    st_nodes += n.InnerText + "\n\n";
            //    //else
            //    //st_nodes += n.InnerText + "\n";
            //}
            //MessageBox.Show(st_nodes);
        }
        public void UpdateCurrSkyImg()
        {
            HtmlNode currTempNode = hd.DocumentNode.SelectSingleNode("//div[@class='current-weather__thermometer current-weather__thermometer_type_now']");
            string currTemp = currTempNode.InnerText;
            CurrentTemp_TextBlock.Text = currTemp;
            HtmlNode currSkyNode = hd.DocumentNode.SelectSingleNode("//span[@class='current-weather__comment']");
            string currSky = currSkyNode.InnerText;

            List<int> tempRangeToday = new List<int>();

            if (DateTime.Now.Hour > 6 && DateTime.Now.Hour < 22)
            {
                if (currSky == "малооблачно" || currSky == "облачно с прояснениями")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/sun_cloud.png", UriKind.Relative));
                else if (currSky == "ясно")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/sun.png", UriKind.Relative));
                else if (currSky == "пасмурно")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/multiClouds.png", UriKind.Relative));
                else if (currSky == "небольшой дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/lightRainSun.png", UriKind.Relative));
                else if (currSky == "дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
                else if (currSky == "сильный дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
                else if (currSky == "сильный дождь, гроза")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
                else if (currSky == "дождь со снегом")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
                else if (currSky == "снег")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));

            }
            else
            {
                if (currSky == "малооблачно" || currSky == "облачно с прояснениями")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/moon_cloud.png"));
                else if (currSky == "ясно")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/moon.png"));
                else if (currSky == "пасмурно")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/b_cloud.png"));
                else if (currSky == "небольшой дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/n_LiRain.png"));
                else if (currSky == "дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
                else if (currSky == "сильный дождь")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
                else if (currSky == "сильный дождь, гроза")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
                else if (currSky == "дождь со снегом")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
                else if (currSky == "снег")
                    currSky_Img.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));
            }
        }
        public void UpdateTempForecast()
        {
            int[] tempRange1Day = new int[]
            {
                TempValues5days[8],
                TempValues5days[9],
                TempValues5days[10],
                TempValues5days[11],
                TempValues5days[12],
                TempValues5days[13],
                TempValues5days[14],
                TempValues5days[15],
            };
            D1Temperature_TextBlock.Text = String.Format("{0:+0;-0;0}...{1:+0;-0;0} °C", tempRange1Day.Min(), tempRange1Day.Max());

            int[] tempRange2Day = new int[]
            {
                TempValues5days[16],
                TempValues5days[17],
                TempValues5days[18],
                TempValues5days[19],
                TempValues5days[20],
                TempValues5days[21],
                TempValues5days[22],
                TempValues5days[23],
            };
            D2Temperature_TextBlock.Text = String.Format("{0:+0;-0;0}...{1:+0;-0;0} °C", tempRange2Day.Min(), tempRange2Day.Max());

            int[] tempRange3Day = new int[]
            {
                TempValues5days[24],
                TempValues5days[25],
                TempValues5days[26],
                TempValues5days[27],
                TempValues5days[28],
                TempValues5days[29],
                TempValues5days[30],
                TempValues5days[31],
            };
            D3Temperature_TextBlock.Text = String.Format("{0:+0;-0;0}...{1:+0;-0;0} °C", tempRange3Day.Min(), tempRange3Day.Max());

            int[] tempRange4Day = new int[]
            {
                TempValues5days[32],
                TempValues5days[33],
                TempValues5days[34],
                TempValues5days[35],
                TempValues5days[36],
                TempValues5days[37],
                TempValues5days[38],
                TempValues5days[39],
            };
            D4Temperature_TextBlock.Text = String.Format("{0:+0;-0;0}...{1:+0;-0;0} °C", tempRange4Day.Min(), tempRange4Day.Max());

            string st_TV5D_Elems = "";
            int count = 0;
            int count1 = 1;
            foreach (var e in TempValues5days)
            {
                if (count1 % 8 == 0)
                    st_TV5D_Elems += count + ". " + e.ToString() + "\n\n";
                else
                    st_TV5D_Elems += count + ". " + e.ToString() + "\n";
                count++;
                count1++;
            }
        }
        public void UpdateWPForecast()
        {
            HtmlNodeCollection weatherConditions = hd.DocumentNode.SelectNodes("//td[@class='weather-table__body-cell weather-table__body-cell_type_condition']/div[@class='weather-table__value']");

            if (weatherConditions[5].InnerText.ToString() == "малооблачно" || weatherConditions[5].InnerText.ToString() == "облачно с прояснениями")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun_cloud.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "ясно")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "пасмурно")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/multiClouds.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "небольшой дождь")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightRainSun.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "дождь")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "сильный дождь")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "сильный дождь, гроза")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "дождь со снегом")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
            else if (weatherConditions[5].InnerText.ToString() == "снег")
                D1Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));
            if (weatherConditions[9].InnerText.ToString() == "малооблачно" || weatherConditions[9].InnerText.ToString() == "облачно с прояснениями")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun_cloud.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "ясно")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "пасмурно")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/multiClouds.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "небольшой дождь")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightRainSun.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "дождь")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "сильный дождь")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "сильный дождь, гроза")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "дождь со снегом")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
            else if (weatherConditions[9].InnerText.ToString() == "снег")
                D2Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));
            if (weatherConditions[13].InnerText.ToString() == "малооблачно" || weatherConditions[13].InnerText.ToString() == "облачно с прояснениями")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun_cloud.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "ясно")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "пасмурно")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/multiClouds.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "небольшой дождь")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightRainSun.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "дождь")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "сильный дождь")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "сильный дождь, гроза")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "дождь со снегом")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
            else if (weatherConditions[13].InnerText.ToString() == "снег")
                D3Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));
            if (weatherConditions[17].InnerText.ToString() == "малооблачно" || weatherConditions[17].InnerText.ToString() == "облачно с прояснениями")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun_cloud.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "ясно")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/sun.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "пасмурно")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/multiClouds.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "небольшой дождь")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightRainSun.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "дождь")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rain.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "сильный дождь")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/heavyRain.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "сильный дождь, гроза")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainElStorm.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "дождь со снегом")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/rainSnow.png", UriKind.Relative));
            else if (weatherConditions[17].InnerText.ToString() == "снег")
                D4Weather_Image.Source = new BitmapImage(new Uri(@"Assets/pics/lightSnow.png", UriKind.Relative));
        }
        public void UpdateWind()
        {
            HtmlNode windDirNode = hd.DocumentNode.SelectSingleNode("//abbr[@class=' icon-abbr']");
            string windDir_Ru = windDirNode.InnerText.ToString();
            string windDir_En = "";
            if (windDir_Ru == "С")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirN.png", UriKind.Relative));
                windDir_En = "N";
            }                
            else if (windDir_Ru == "СЗ")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirNW.png", UriKind.Relative));
                windDir_En = "NW";
            }                
            else if (windDir_Ru == "З")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirW.png", UriKind.Relative));
                windDir_En = "W";
            }                
            else if (windDir_Ru == "ЮЗ")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirSW.png", UriKind.Relative));
                windDir_En = "SW";
            }                
            else if (windDir_Ru == "Ю")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirS.png", UriKind.Relative));
                windDir_En = "S";
            }                
            else if (windDir_Ru == "ЮВ")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirSE.png", UriKind.Relative));
                windDir_En = "SE";
            }                
            else if (windDir_Ru == "В")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirE.png", UriKind.Relative));
                windDir_En = "E";
            }
                
            else if (windDir_Ru == "СВ")
            {
                Wind_img.Source = new BitmapImage(new Uri(@"Assets/pics/dirNE.png", UriKind.Relative));
                windDir_En = "NE";
            }                

            HtmlNode windSpeedNode = hd.DocumentNode.SelectSingleNode("//span[@class='wind-speed']");
            string windSpeed = windSpeedNode.InnerText.ToString();
            Regex regex = new Regex(@"\d+.\d+");
            Match match = regex.Match(windSpeed);
            CurrentWindSpeed_TextBlock.Text = windDir_En + " " + match.Value.ToString() + " m/s";
        }
    }
}
